import configparser
import datetime
import locale
import logging.handlers
import sys
import threading
import time
from queue import Queue

import kivy
from kivy.animation import Animation
from kivy.app import App
from kivy.config import Config
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import AsyncImage
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.relativelayout import RelativeLayout

from fibaro import outtemp
from pelletalarm import PelletAlarm

kivy.require('1.10.1')  # replace with your current kivy version !

logger = logging.getLogger('dashboard')
logger.setLevel(logging.INFO)

# add handler to the logger
handler = logging.StreamHandler(sys.stdout)
# try:
#     handler = logging.handlers.SysLogHandler('/dev/log')
# except AttributeError:
#     print("fallback to std logging")

formatter = logging.Formatter(
    'FibaroDashboard: { "loggerName":"%(name)s", "asciTime":"%(asctime)s", "pathName":"%(pathname)s", "logRecordCreationTime":"%(created)f", "functionName":"%(funcName)s", "levelNo":"%(levelno)s", "lineNo":"%(lineno)d", "time":"%(msecs)d", "levelName":"%(levelname)s", "message":"%(message)s"}')

handler.formatter = formatter
logger.addHandler(handler)

logger.info("Start")


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


sys.excepthook = handle_exception


class Dashboard(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.layout = GridLayout(cols=2)

        self.image = AsyncImage(pos_hint={'center_x': 0.7, 'center_y': 0.75}, allow_stretch=True, size_hint=(0.4, 0.4))
        self.label_temp = Label(pos_hint={'center_x': 0.4, 'center_y': 0.4})
        self.pellet_alarm = PelletAlarm(pos_hint={'center_x': 0.2, 'center_y': 0.2})

        self.image1 = AsyncImage(pos_hint={'center_x': 0.5, 'center_y': 1}, allow_stretch=True, size_hint=(1, 1))
        self.label_temp1 = Label(pos_hint={'center_x': 0.5, 'center_y': 0.5})
        self.image2 = AsyncImage(pos_hint={'center_x': 0.5, 'center_y': 1}, allow_stretch=True, size_hint=(1, 1))
        self.label_temp2 = Label(pos_hint={'center_x': 0.5, 'center_y': 0.5})
        self.image3 = AsyncImage(pos_hint={'center_x': 0.5, 'center_y': 1}, allow_stretch=True, size_hint=(1, 1))
        self.label_temp3 = Label(pos_hint={'center_x': 0.5, 'center_y': 0.5})

        self.label_clock = Label(pos_hint={'center_x': 0.5, 'center_y': 0.6})
        self.label_day = Label(pos_hint={'center_x': 0.5, 'center_y': 0.35})
        self.label_date = Label(pos_hint={'center_x': 0.5, 'center_y': 0.13})

        self.popup_label = Label(pos_hint={'center_x': 0.5, 'center_y': 0.5})
        self.popup = Popup(title="",
                           content=self.popup_label,
                           size_hint=(0.5, 0.15),
                           # size=(100, 50),
                           pos_hint={'right': 0.5, 'top': 0.9})

        self.button = Button(text='Köögi valgus', size_hint=(0.5, 0.5), pos_hint={'center_x': 0.5, 'center_y': 0.5})
        self.button.bind(height=Dashboard.resize_button_lbl)
        self.button.bind(on_press=self.popup_show)
        self.button.bind(on_release=self.popup_close)

        self.light_trigger = threading.Thread(target=self.light_event, daemon=True)
        self.light_trigger_break = True
        self.counter = 0

        t = threading.Thread(target=screensaver, args=(self,), daemon=True)
        t.start()

    def read_data(self):
        t = threading.Thread(target=temperature_from_q, args=(self.label_temp,))
        t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
        t.start()

        t = threading.Thread(target=pellet_from_q, args=(self.pellet_alarm,))
        t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
        t.start()

        t = threading.Thread(target=temp_icon_from_q, args=(self.image,))
        t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
        t.start()

        t = threading.Thread(target=forecast_from_q, args=(
            self.label_temp1, self.label_temp2, self.label_temp3, self.image1, self.image2, self.image3,))
        t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
        t.start()

        t = threading.Thread(target=clock_polling, args=(self.label_clock, self.label_day, self.label_date))
        t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
        t.start()

    @staticmethod
    def resize_text(instance, value):
        instance.font_size = value * 0.4

    @staticmethod
    def resize_forecast_text(instance, value):
        instance.font_size = value * 0.3

    @staticmethod
    def resize_clock(instance, value):
        instance.font_size = value * 0.35

    @staticmethod
    def resize_date(instance, value):
        instance.font_size = value * 0.2

    @staticmethod
    def resize_day(instance, value):
        instance.font_size = value * 0.2

    @staticmethod
    def resize_button_lbl(instance, value):
        instance.font_size = value * 0.2

    def temp_block(self):
        layout = RelativeLayout()
        self.label_temp.bind(height=self.resize_text)

        layout.add_widget(self.label_temp)
        layout.add_widget(self.image)
        return layout

    def pellet_block(self):
        layout = RelativeLayout()
        layout.add_widget(self.pellet_alarm)
        return layout

    def forecast_block(self):
        layout = GridLayout(cols=3, rows=2)

        self.label_temp1.bind(height=self.resize_forecast_text)
        self.label_temp2.bind(height=self.resize_forecast_text)
        self.label_temp3.bind(height=self.resize_forecast_text)

        layout.add_widget(self.label_temp1)
        layout.add_widget(self.label_temp2)
        layout.add_widget(self.label_temp3)

        layout_ico1 = RelativeLayout()
        layout_ico2 = RelativeLayout()
        layout_ico3 = RelativeLayout()

        layout.add_widget(layout_ico1)
        layout.add_widget(layout_ico2)
        layout.add_widget(layout_ico3)

        layout_ico1.add_widget(self.image1)
        layout_ico2.add_widget(self.image2)
        layout_ico3.add_widget(self.image3)
        return layout

    def time_block(self):
        layout = RelativeLayout()
        self.label_clock.bind(height=self.resize_clock)
        self.label_date.bind(height=self.resize_date)
        self.label_day.bind(height=self.resize_day)

        layout.add_widget(self.label_clock)
        layout.add_widget(self.label_day)
        layout.add_widget(self.label_date)
        return layout

    def build(self):
        first_block = FloatLayout()

        temp_block = self.temp_block()
        temp_block.pos_hint = {'x': .01, 'y': 0}

        pellet_block = self.pellet_block()
        pellet_block.pos_hint = {'x': .1, 'y': 0.7}
        pellet_block.size_hint = (.5, .5)

        first_block.add_widget(temp_block)
        first_block.add_widget(pellet_block)

        self.layout.add_widget(first_block)
        self.layout.add_widget(self.time_block())
        self.layout.add_widget(self.forecast_block())
        layout_btn = RelativeLayout()
        layout_btn.add_widget(self.button)
        self.layout.add_widget(layout_btn)

        self.read_data()
        return self.layout

    def light_event(self):
        self.counter = 1
        self.light_trigger_break = False
        while not self.light_trigger_break:
            counter_text = '|' * self.counter
            if self.counter < 21:
                self.counter += 1
                time.sleep(0.5)
                self.popup_label.text = counter_text
                if self.counter > 3:
                    outtemp.ceiling_brightness(config, self.counter)

    def popup_show(self, button):
        print(button)
        self.popup.open()
        self.popup_label.text = ''
        self.light_trigger = threading.Thread(target=self.light_event, daemon=True)
        self.light_trigger.start()

    def popup_close(self, button):
        print(button)
        self.popup.dismiss()
        self.light_trigger_break = True
        if self.counter < 3:
            outtemp.ceiling_toggle(config)


fibaro_temperature_queue = Queue()
fibaro_pellet_queue = Queue()
q2 = Queue()
q3 = Queue()


def clock_polling(label_clock, label_day, label_date):
    while True:
        now = datetime.datetime.now()
        label_clock.text = now.strftime("%X")
        label_day.text = now.strftime("%A")
        label_date.text = now.strftime("%x")
        time.sleep(0.5)


def temperature_from_q(label):
    while True:
        temp = fibaro_temperature_queue.get()
        if temp:
            logger.info("Temperature from q {}".format(temp))
            label.text = temp


def pellet_from_q(label):
    while True:
        distance = fibaro_pellet_queue.get()
        if distance:
            logger.info("Pellet distance from q {}".format(distance))
            label.value = distance


def forecast_from_q(label1, label2, label3, image1, image2, image3):
    while True:
        forecast = q3.get()
        d1 = forecast['list'][0]
        d2 = forecast['list'][1]
        d3 = forecast['list'][2]

        label1.text = str(d1['temp']['day'])
        label2.text = str(d2['temp']['day'])
        label3.text = str(d3['temp']['day'])

        image1.source = d1['weather'][0]['icon']
        image2.source = d2['weather'][0]['icon']
        image3.source = d3['weather'][0]['icon']


def screensaver(dashboard):
    while True:
        time.sleep(300)
        anim = Animation(opacity=0, duration=2.)
        anim.start(dashboard.layout)
        time.sleep(3)
        anim = Animation(opacity=1, duration=2.)
        anim.start(dashboard.layout)


def temp_icon_from_q(image):
    while True:
        icon = q2.get()
        image.source = icon


def collect_temperature():
    def collector():
        while True:
            logger.info("poll data!")

            fibaro_temperature_queue.put(outtemp.get_outside_temp(config))
            fibaro_temperature_queue.task_done()

            fibaro_pellet_queue.put(outtemp.get_pellet_distance(config))
            fibaro_pellet_queue.task_done()

            icon = outtemp.get_icon(config)
            if icon:
                q2.put(icon)
                q2.task_done()

            forecast = outtemp.get_forecast(config)
            if forecast:
                q3.put(forecast)
                q3.task_done()

            time.sleep(float(config['DEFAULT']['poll_interval']))

    t = threading.Thread(target=collector)
    t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
    t.start()
    return t


if __name__ == '__main__':
    locale = locale.setlocale(locale.LC_ALL, '')
    config = configparser.ConfigParser()
    config.read('/opt/projects/fibaro-dashboard/configuration.ini')

    Config.set('graphics', 'fullscreen', config['DEFAULT']['fullscreen'])


    def worker():
        collector_thread = None
        while True:
            if not collector_thread or not collector_thread.is_alive():
                collector_thread = collect_temperature()
            time.sleep(float(60))


    t = threading.Thread(target=worker)
    t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
    t.start()

    Dashboard().run()
