import logging

import requests
from requests.auth import HTTPBasicAuth
from requests.exceptions import RequestException

logger = logging.getLogger('dashboard')


def ceiling_toggle(config):
    try:
        config_fibaro = config['fibaro']
        if __call_api("/api/devices/171", config_fibaro).json()['properties']['brightness'] == '0':
            body = {"args": [255, 255, 255, 0]}
            return (__post_api('/api/devices/171/action/setColor', config_fibaro, body).json(),
                    __post_api('/api/devices/94/action/setColor', config_fibaro, body).json())
        else:
            body = {"args": []}
            return (__post_api('/api/devices/171/action/turnOff', config_fibaro, body).json(),
                    __post_api('/api/devices/94/action/turnOff', config_fibaro, body).json())
    except Exception as err:
        logger.error('connection error to fibaro: {}'.format(err))


def ceiling_brightness(config, brightness):
    try:
        config_fibaro = config['fibaro']
        body = {"args": [brightness * 5]}
        return (__post_api('/api/devices/171/action/setBrightness', config_fibaro, body).json(),
                __post_api('/api/devices/94/action/setBrightness', config_fibaro, body).json())
    except Exception as err:
        logger.error('connection error to fibaro: {}'.format(err))


def __call_api(api_str, config_fibaro):
    resp = requests.get(
        config_fibaro['uri'] + api_str,
        auth=HTTPBasicAuth(config_fibaro['user'],
                           config_fibaro['pw']),
        timeout=30.)
    logger.info('api called {}'.format(resp))
    if resp.status_code != 200:
        logger.error('{} {}'.format(api_str, resp.status_code))
    return resp


def __post_api(api_str, config_fibaro, body=None):
    resp = requests.post(
        config_fibaro['uri'] + api_str,
        json=body,
        auth=HTTPBasicAuth(config_fibaro['user'],
                           config_fibaro['pw']),
        timeout=5.)
    if resp.status_code != 202:
        logger.error('{} {}'.format(api_str, resp.status_code))
    return resp


def get_outside_temp(config):
    try:
        logger.info("Get outside temp")
        outside_temp = __call_api('/api/devices/353', config['fibaro']).json()['properties']['value']
        logger.info("Outside temp: {}".format(outside_temp))
        return outside_temp
    except Exception as err:
        logger.error('connection error to fibaro: {}'.format(err))

def get_pellet_distance(config):
    try:
        logger.info("Get outside temp")
        value = __call_api('/api/devices/212', config['fibaro']).json()['properties']['ui.PelletDistance.value']
        logger.info("Pellet distance value: {}".format(value))
        return value
    except Exception:
        print('connection error to fibaro')


def get_icon(config):
    logger.info("Get weather icon")
    try:
        config_map = config['openweathermap']
        city_id = config_map['city_id']
        resp = requests.get('http://api.openweathermap.org/data/2.5/weather?id={0}&appid={1}'
                            .format(city_id, config_map['api_id']), timeout=5.)
        if resp.status_code != 200:
            logger.info('GET /api.openweathermap.org/data/2.5/weather?id={} {}'.format(city_id, resp.status_code))
        return 'http://openweathermap.org/img/w/' + resp.json()['weather'][0]['icon'] + '.png'
    except RequestException as err:
        logger.error('connection error to weather server! {}'.format(err))
    except Exception as err:
        logger.error('Unknown error {}'.format(err))
    return None


def get_forecast(config):
    try:
        config_map = config['openweathermap']
        city_id = config_map['city_id']
        api_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?id={0}&appid={1}&units=metric&mode=json'
        resp = requests.get(api_url.format(city_id, config_map['api_id']), timeout=5.)
        if resp.status_code != 200:
            logger.info('GET /api.openweathermap.org/data/2.5/forecast/daily/?id={} {}'.format(city_id, resp.status_code))
        json = resp.json()
        for val in json['list']:
            icon = val['weather'][0]['icon']
            val['weather'][0]['icon'] = 'http://openweathermap.org/img/w/' + icon + '.png'
        return json
    except ConnectionError as err:
        logger.error('connection error to weather server! {}'.format(err))
    except Exception as err:
        logger.error('Unknown error {}'.format(err))
    return None
