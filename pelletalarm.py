from kivy.graphics.context_instructions import Color
from kivy.graphics.instructions import InstructionGroup
from kivy.graphics.vertex_instructions import Ellipse
from kivy.properties import NumericProperty
from kivy.uix.widget import Widget
from kivy.animation import Animation


class PelletAlarm(Widget):
    value = NumericProperty(0)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.anim = Animation(opacity=0.1, duration=0.2)
        self.anim += Animation(opacity=0.6, duration=0.2)
        self.anim += Animation(opacity=1, duration=0.3)
        self.anim += Animation(opacity=0.6, duration=0.2)
        self.anim += Animation(opacity=0.1, duration=0.2)
        self.anim.repeat = True

        with self.canvas:
            Ellipse()

    def green_alarm(self):
        blue = InstructionGroup()
        blue.add(Color(0, 1, 0, 1))
        blue.add(self.instruction_ellipse())

    def yellow_alarm(self):
        blue = InstructionGroup()
        blue.add(Color(1, 1, 0, 1))
        blue.add(self.instruction_ellipse())

    def red_alarm(self):
        blue = InstructionGroup()
        blue.add(Color(1, 0, 0, 1))
        blue.add(self.instruction_ellipse())

    def instruction_ellipse(self):
        ellipse = Ellipse()
        ellipse.size = [self.height * 0.5, self.height * 0.5]
        return ellipse

    def draw_alarm(self):
        with self.canvas:
            self.anim.cancel(self)
            self.canvas.clear()

            if self.value <= 50:
                self.green_alarm()
            elif 70 >= self.value > 50:
                self.yellow_alarm()
            elif 80 >= self.value > 70:
                self.red_alarm()
            elif self.value > 80:
                self.red_alarm()
                self.anim.start(self)

    def on_value(self, instance, value):
        print('Pellet Distance {pos}'.format(pos=value))
        self.draw_alarm()

    def on_height(self, instance, value):
        print('Height {pos}'.format(pos=value))
        self.draw_alarm()